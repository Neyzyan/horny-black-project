const canvas = document.querySelector("#gamewindow")
const ctx = canvas.getContext("2d")

const DELAY = 10
const BALL_DEFAULT_X = canvas.width / 2
const BALL_DEFAULT_Y = canvas.height - 30
const PADDLE_DEFAULT_X = 600
const PADDLE_DEFAULT_WIDTH = 250


let brickRowCount = 3;
let brickColumnCount = 5;
let brickWidth = 187.5;
let brickHeight = 50;
let brickPadding = 25;
let brickOffsetTop = 75;
let brickOffsetLeft = 75;
let rightPressed = false;
let leftPressed = false;
let score = 0;

let bricks = [];
for(let c = 0; c < brickColumnCount; c++) {
    bricks[c] = [];
    for(let r = 0; r < brickRowCount; r++) {
        bricks[c][r] = { x: 0, y: 0, status: 1 };
    }
}


document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
document.addEventListener("mousemove", mouseMoveHandler, false);

function keyDownHandler(e) {
    if(e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = true;
    }
    else if(e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = true;
    }
}


function keyUpHandler(e) {
    if(e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = false;
    }
    else if(e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = false;
    }
}


function mouseMoveHandler(e) {
    let relativeX = e.clientX - canvas.offsetLeft;
    if(relativeX > 0 && relativeX < canvas.width) {
        paddle.x = relativeX - paddle.width/2;
    }
}



function collisionDetection() {
    for (let c = 0; c < brickColumnCount; c++) {
        for (let r = 0; r < brickRowCount; r++) {
            let b = bricks[c][r];
            if (b.status == 1) {
                if (ball.x > b.x && ball.x < b.x + brickWidth && ball.y > b.y && ball.y < b.y + brickHeight) {
                    ball.dy = -ball.dy;
                    b.status = 0;
                    score++;
                    if(score == brickRowCount*brickColumnCount) {
                        alert("ЛУЧШИЙ!");
                        document.location.reload();
                        clearInterval(interval);
                    }
                }
            }
        }
    }
}


let ball = {
    x: BALL_DEFAULT_X,
    y: BALL_DEFAULT_Y,
    dx: 7,
    dy: 7,
    radius: 19,
    color: "#7c2ff7",
}


let paddle = {
    x:  (canvas.width - PADDLE_DEFAULT_WIDTH)/2 ,
    width: PADDLE_DEFAULT_WIDTH,
    height: 25,
    color: "#7c2ff7",
}


function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
}


function drawBall() {
    ctx.beginPath()
    ctx.arc(ball.x, ball.y, ball.radius, 0, Math.PI * 2)
    ctx.fillStyle = ball.color
    ctx.fill()
    ctx.closePath()   
}   


function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddle.x , canvas.height - paddle.height, paddle.width, paddle.height);
    ctx.fillStyle = paddle.color;
    ctx.fill();
    ctx.closePath();
}


function drawScore() {
    ctx.font = "24px serif";
    ctx.fillStyle = "#7c2ff7";
    ctx.fillText("Score: "+score, 8, 20);
}


function nextTick(){
    ball.x += ball.dx
    ball.y += ball.dy
}


function ballRebound() {
    if(ball.x + ball.dx > canvas.width - ball.radius || ball.x + ball.dx < ball.radius) {
        ball.dx = -ball.dx;
    }
    if(ball.y + ball.dy < ball.radius) {
        ball.dy = -ball.dy;
    }
    else if(ball.y + ball.dy > canvas.height - ball.radius) {
        if(ball.x > paddle.x && ball.x < paddle.x + paddle.width) {
            ball.dy = -ball.dy;
        }
        else {
            alert("GAME OVER");
            document.location.reload();
            clearInterval(interval);
        }
    }
}


function paddleMove() {
    if(rightPressed && paddle.x < canvas.width - paddle.width) {
        paddle.x += 9;
    }
    else if(leftPressed && paddle.x > 0) {
        paddle.x -= 9;
    }
}

function drawBricks() { 
    for(let c = 0; c < brickColumnCount; c++) {
        for(let r = 0; r < brickRowCount; r++) {
            if(bricks[c][r].status == 1) {
                let brickX = (c * (brickWidth+brickPadding)) + brickOffsetLeft;
                let brickY = (r * (brickHeight+brickPadding)) + brickOffsetTop;
                bricks[c][r].x = brickX;
                bricks[c][r].y = brickY;
                ctx.beginPath();
                ctx.rect(brickX, brickY, brickWidth, brickHeight);
                ctx.fillStyle = "#7c2ff7";
                ctx.fill();
                ctx.closePath();
            }
        }   
    }
}

function draw() {
    clearCanvas()
    drawBricks()
    drawBall()
    drawPaddle()
    drawScore()
    collisionDetection()
    ballRebound()
    paddleMove()
    nextTick()
}


let interval = setInterval(draw, DELAY)

